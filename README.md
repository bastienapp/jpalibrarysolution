# Spring Boot - Spring Data JPA

Approfondis tes connaissances de Spring Data JPA

## Ressources

- [Veille - Différence entre JPA et Hibernate](https://waytolearnx.com/2019/03/difference-entre-jpa-et-hibernate.html)
- [Veille - Demystifying Database Access in Java: JPA, Hibernate, JDBC, and Spring Data](https://www.linkedin.com/pulse/demystifying-database-access-java-jpa-hibernate-jdbc-ali-gb7uf)
- [OpenClassrooms - Créez une application Java avec Spring Boot](https://openclassrooms.com/fr/courses/6900101-creez-une-application-java-avec-spring-boot/7078007-creez-lapi-avec-les-bons-starters)
- [Installer et utiliser Postman](https://www.youtube.com/watch?v=vMdhZvmRPe0)

## Contexte du projet

Tu connais les bases de la création d'une API REST, ton entreprise souhaite maintenant que tu te perfectionnes dans l'utilisation de Spring Data JPA.

Tu vas ainsi découvrir :

- ce qu'est un ORM
- comment Spring utilise Spring Data JPA, Hibernate et JDBC
- comment mettre en place des relations entre des entités

Avant tout, il t'est demandé de rédiger un sujet de veille répondant aux questions suivantes :

- Qu'est-ce un ORM ?
- Qu'est-ce que Hibernate ?
- Qu'est-ce que Spring Data JPA, quel est son rapport avec Hibernate ?
- Qu'est-ce que JDBC, quel est son rapport avec Hibernate ?

Tu as des liens en ressource pour t'aider à y répondre.

Une fois ta veille terminée (il faudra l'ajouter en solution de ce brief), ton entreprise te propose de voir comment mettre en relation des entités (one-to-many, many-to-many...) grâce à la ressource suivante : [Les relations avec JPA](https://gayerie.dev/epsi-b3-orm/javaee_orm/jpa_relations.html).

Enfin, tu découvriras comment ajouter des fonctions personnalisées dans un *Repository* dans cette ressource : [Ajout de méthodes dans une interface de repository](https://gayerie.dev/docs/spring/spring/spring_data_jpa.html#ajout-de-methodes-dans-une-interface-de-repository).

Une fois les ressources étudiées, ton entreprise te demande de reprendre ton API REST `jpalibrary` en y apportant les modifications suivantes :

- Ajoute les entités :
  - Author avec un identifiant (Long) auto-généré, un nom (String) et un prénom (String)
  - Category (avec un identifiant (Long) auto-généré et un nom)
- Ajoute les annotations nécessaires à la création des relations entres Book et Category : un livre possède une seule catégorie, une catégorie peut appartenir à plusieurs livres.
- Ajoute les annotations nécessaires à la création des relations entres Book et Author : un livre peut posséder plusieurs auteurs, un auteur peut écrire plusieurs livres
- Ajoute des données de test et vérifie avec Postman que les informations apparaissent bien

## Modalités pédagogiques

En autonomie, sur deux jours

## Modalités d'évaluation

Il sera vérifié que le programme s'exécute que le CRUD est fonctionnel à partir de Postman.

## Livrables

- Un lien vers le sujet de veille
- Un lien GitLab vers le projet

## Critères de performance

- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions