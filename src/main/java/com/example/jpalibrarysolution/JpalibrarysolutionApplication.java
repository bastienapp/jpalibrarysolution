package com.example.jpalibrarysolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpalibrarysolutionApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpalibrarysolutionApplication.class, args);
	}

}
