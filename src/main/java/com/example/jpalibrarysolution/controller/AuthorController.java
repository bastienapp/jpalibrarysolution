package com.example.jpalibrarysolution.controller;

import org.springframework.web.bind.annotation.RestController;

import com.example.jpalibrarysolution.entity.Author;
import com.example.jpalibrarysolution.repository.AuthorRepo;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    private AuthorRepo authorRepo;

    public AuthorController(AuthorRepo authorRepoInjected) {
        this.authorRepo = authorRepoInjected;
    }

    @GetMapping("")
    public List<Author> getAll() {
        return this.authorRepo.findAll();
    }

    @PostMapping("")
    public Author postOne(@RequestBody Author author) {

        return this.authorRepo.save(author);
    }

}
