package com.example.jpalibrarysolution.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.jpalibrarysolution.entity.Book;
import com.example.jpalibrarysolution.entity.Category;
import com.example.jpalibrarysolution.repository.BookRepo;
import com.example.jpalibrarysolution.repository.CategoryRepo;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("/books")
public class BookController {

    private BookRepo bookRepo;
    private CategoryRepo categoryRepo;

    public BookController(
        BookRepo bookRepoInjected,
        CategoryRepo categoryRepoInjected
    ) {
        this.bookRepo = bookRepoInjected;
        this.categoryRepo = categoryRepoInjected;
    }

    @GetMapping("")
    public List<Book> getAllBooks() {
        return this.bookRepo.findAll();
    }

    // /books/1 -> PathVariable : une vriable directement dans le chemin
    // /books?id=1 -> RequestParam : paramètre de requête (après un point d'interrogation)
    @GetMapping("/{bookId}")
    public ResponseEntity<Book> getBookById(@PathVariable Long bookId) {
        var optionalBook = this.bookRepo.findById(bookId);
        if (optionalBook.isPresent()) { // .isPresent() : si ma valeur existe
            var book = optionalBook.get(); // .get() : récupère la valeur
            return ResponseEntity.ok(book);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<Boolean> deleteBookById(@PathVariable Long bookId) {
        var optionalBook = this.bookRepo.findById(bookId);
        if (optionalBook.isPresent()) {
            this.bookRepo.deleteById(bookId);
            return ResponseEntity.ok(true);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public Book postBook(@RequestBody Book book) {

        return this.bookRepo.save(book);
    }

    @PutMapping("/{bookId}")
    public ResponseEntity<Book> updateBook(@PathVariable Long bookId, @RequestBody Book bookData) {

        var optionalBook = this.bookRepo.findById(bookId);
        if (optionalBook.isPresent()) {
            var existingBook = optionalBook.get(); // je récupère le livre tel qu'il est actuellement en BDD
            if (bookData.getTitle() != null && !bookData.getTitle().isEmpty()) {
                existingBook.setTitle(bookData.getTitle());
            }
            if (bookData.getDescription() != null && !bookData.getDescription().isEmpty()) {
                existingBook.setDescription(bookData.getDescription());
            }
            if (bookData.isAvailable() != null) {
                existingBook.setAvailable(bookData.isAvailable());
            }
            return ResponseEntity.ok(this.bookRepo.save(existingBook));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/title/{titleContent}")
    public List<Book> findByTitle(@PathVariable(name = "titleContent") String title) {
        return this.bookRepo.findByTitleContainingIgnoreCase(title);
    }

    // associer une catégorie à un livre
    @PutMapping("/{bookId}/categories/{categoryId}")
    public ResponseEntity<Book> addCategoryToBook(@PathVariable Long bookId, @PathVariable Long categoryId) {

        Optional<Book> bookOptional = this.bookRepo.findById(bookId);
        Optional<Category> categoryOptional = this.categoryRepo.findById(categoryId);
        if (bookOptional.isPresent() && categoryOptional.isPresent()) {
            Book book = bookOptional.get();
            Category category = categoryOptional.get();
            book.setCategory(category);
            Book bookUpdated = this.bookRepo.save(book);
            return ResponseEntity.ok(bookUpdated);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
