package com.example.jpalibrarysolution.controller;

import org.springframework.web.bind.annotation.RestController;

import com.example.jpalibrarysolution.entity.Category;
import com.example.jpalibrarysolution.repository.CategoryRepo;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryRepo categoryRepo;

    public CategoryController(CategoryRepo categoryRepoInjected) {
        this.categoryRepo = categoryRepoInjected;
    }

    @GetMapping("")
    public List<Category> getAll() {
        return this.categoryRepo.findAll();
    }

    @PostMapping("")
    public Category postOne(@RequestBody Category category) {

        return this.categoryRepo.save(category);
    }

}
