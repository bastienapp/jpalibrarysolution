package com.example.jpalibrarysolution.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jpalibrarysolution.entity.Author;

@Repository
public interface AuthorRepo extends JpaRepository<Author, Long> {

}
