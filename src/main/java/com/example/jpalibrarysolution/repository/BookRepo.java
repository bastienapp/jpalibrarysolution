package com.example.jpalibrarysolution.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jpalibrarysolution.entity.Book;

@Repository
public interface BookRepo extends JpaRepository<Book, Long> {

    public List<Book> findByTitleContainingIgnoreCase(String title);
}
