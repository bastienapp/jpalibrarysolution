package com.example.jpalibrarysolution.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.jpalibrarysolution.entity.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long> {

    public List<Category> findByNameContainingIgnoreCase(String name);
}
